#include "TaskManager.h"


TaskManager::TaskManager(const std::string& filename) {
    ReadFromFile(filename);
    Shuffle();
}


void TaskManager::ReadFromStream(std::istream* in) {
    while (!(in -> eof())) {
        std::string word;
        std::string correct;
        std::string key;
        (*in) >> word >> correct >> key;
        
        if (!word.empty()) {
            unusedTasks_.emplace_back(word, correct, key);
        }
    }
}


void TaskManager::ReadFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (file.is_open()) {
        ReadFromStream(&file);
        file.close();
    }
}


void TaskManager::SaveToStream(std::ostream* out) const {
    for (auto& task: unusedTasks_) {
        (*out) << task.ToString() << std::endl;
    }
    for (auto& task: usedTasks_) {
        (*out) << task.ToString() << std::endl;
    }
}


void TaskManager::SaveToFile(const std::string& filename) const {
    std::ofstream file(filename);
    if (file.is_open()) {
        SaveToStream(&file);
        file.close();
    }
}


void TaskManager::AddTask(const WordTask& task) {
    unusedTasks_.push_back(task);
}


WordTask TaskManager::GetTask() {
    if (unusedTasks_.size() == 0) {
        Transfuse();
    }
    
    WordTask task = unusedTasks_.back();
    unusedTasks_.pop_back();
    usedTasks_.push_back(task);
    
    return task;
}


void TaskManager::Shuffle() {
    srand(time(nullptr));
    std::random_shuffle(usedTasks_.begin(), usedTasks_.end());
    std::random_shuffle(unusedTasks_.begin(), unusedTasks_.end());
}


size_t TaskManager::size() const {
    return usedTasks_.size() + unusedTasks_.size();
}


void TaskManager::Transfuse() {
    while (usedTasks_.size() > 0) {
        unusedTasks_.push_back(usedTasks_.back());
        usedTasks_.pop_back();
    }
    
    Shuffle();
}







