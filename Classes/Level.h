#pragma once

#include "TaskManager.h"

#include <string>
#include <fstream>
#include <ostream>
#include <istream>


class Level {
public:
    Level();
    Level(const std::string& filename);
    Level(const Level&) = delete;
    Level(Level&&) = default;
    ~Level() = default;
    
    
    int getScorePerTask() const;
    
    
    void ReadFromStream(std::istream* in);
    void ReadFromFile(const std::string& filename);
    void SaveToStream(std::ostream* out) const;
    void SaveToFile(const std::string& filename) const;
    
    std::pair<float, WordTask> getNextTask();
    
    
    Level& operator = (const Level&) = delete;
    Level& operator = (Level&&) = default;
    
private:
    int times_;
    int scorePerTask_, timesPerStep_;
    float duration_, durationStep_;
    TaskManager taskManager_;
};
