#pragma once

#include "Level.h"


class GameState {
public:
    GameState();
    GameState(const GameState&) = delete;
    GameState(GameState&&) = default;
    ~GameState() = default;
    
    size_t getScore() const;
    bool isAlive() const;
    int getHealthPoints() const;
    void setHealthPoints(int healthPoints);
    Level& getLevel();
    
    
    void hit(int damage);
    void updateScore(size_t cash);
    void reset();
    
    
    void Save(); // TODO
    void Load(); // TODO
    
    GameState& operator = (const GameState&) = delete;
    GameState& operator = (GameState&&) = default;
    
private:
    size_t score_;
    int healthPoints_;
    Level level_;
};
