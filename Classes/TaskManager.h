#pragma once

#include "WordTask.h"
#include "time.h"

#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <ostream>
#include <istream>

class TaskManager {
public:
    TaskManager() = default;
    TaskManager(const std::string& filename);
    TaskManager(const TaskManager&) = default;
    TaskManager(TaskManager&&) = default;
    ~TaskManager() = default;
    
    void ReadFromStream(std::istream* in);
    void ReadFromFile(const std::string& filename);
    void SaveToStream(std::ostream* out) const;
    void SaveToFile(const std::string& filename) const;
    
    void AddTask(const WordTask& task);
    WordTask GetTask();
    void Shuffle();
    
    
    size_t size() const;
    void Transfuse();
    
    
    TaskManager& operator = (const TaskManager&) = default;
    TaskManager& operator = (TaskManager&&) = default;
    
private:
    std::vector<WordTask> unusedTasks_;
    std::vector<WordTask> usedTasks_;
};
