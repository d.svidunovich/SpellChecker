#pragma once

#include "cocos2d.h"

#include <string>


class WordTask {
public:
    WordTask() = default;
    WordTask(const std::string& word, const std::string& correct,
             const std::string& key);
    WordTask(const WordTask&) = default;
    WordTask(WordTask&&) = default;
    ~WordTask() = default;
    
    
    const std::string& getWord() const;
    void setWord(const std::string& word);
    
    const std::string& getCorrect() const;
    void setCorrect(const std::string& correct);
    
    const std::string& getKey() const;
    void setKey(const std::string& key);
    
    bool isKeyCorrect(const std::string& key) const;
    bool isKeyCorrect(cocos2d::EventKeyboard::KeyCode keyCode) const;
    
    std::string ToString() const;

    WordTask& operator = (const WordTask&) = default;
    WordTask& operator = (WordTask&&) = default;
    
private:
    std::string word_;
    std::string correct_;
    std::string key_;
    // typeof(key_) == string, to have opportunity for longer keys
};
