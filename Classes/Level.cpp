#include "Level.h"


Level::Level(): times_(0) {}


Level::Level(const std::string& filename): times_(0) {
    ReadFromFile(filename);
}


int Level::getScorePerTask() const {
    return scorePerTask_;
}

void Level::ReadFromStream(std::istream* in) {
    (*in) >> scorePerTask_ >> timesPerStep_ >> duration_ >> durationStep_;
    taskManager_.ReadFromStream(in);
}


void Level::ReadFromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (file.is_open()) {
        ReadFromStream(&file);
        file.close();
    }
}


void Level::SaveToStream(std::ostream* out) const {
    (*out) << scorePerTask_ << " " << timesPerStep_ << " " <<
        duration_ << " " << durationStep_ << std::endl;
    taskManager_.SaveToStream(out);
}


void Level::SaveToFile(const std::string& filename) const {
    std::ofstream file(filename);
    if (file.is_open()) {
        SaveToStream(&file);
        file.close();
    }
}


std::pair<float, WordTask> Level::getNextTask() {
    if (times_ >= timesPerStep_) {
        duration_ -= durationStep_;
        times_ %= timesPerStep_;
    }
    
    return std::make_pair(duration_, taskManager_.GetTask());
}
