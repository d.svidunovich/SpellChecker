#include "GameScene.h"

USING_NS_CC;


const Color3B Game::RIGHT_ANSWER_COLOR  = cocos2d::Color3B(150, 250, 0);

Scene* Game::createScene()
{
    auto scene = Scene::create();
    auto layer = Game::create();
    
    scene->addChild(layer);
    
    return scene;
}


bool Game::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto listener = EventListenerKeyboard::create();
    listener -> onKeyPressed = CC_CALLBACK_2(Game::onKeyPressed, this);
    _eventDispatcher -> addEventListenerWithSceneGraphPriority(listener, this);

    auto background = CreateBackground("Images/background.jpg");
    addChild(background, -50);
    
    // TODO: Pre-load music?
    auto music = CocosDenshion::SimpleAudioEngine::getInstance();
    music -> setBackgroundMusicVolume(0.2);
    music -> playBackgroundMusic("Sounds/background.wav", true);
    maxScore_ = 0;
    
    InitializeGameNodes();
    updateTask();
    schedule(SEL_SCHEDULE(&Game::FindDeadTasks), 0.1);

    return true;
}


Sprite* Game::CreateBackground(const std::string& filepath) {
    auto background = Sprite::create(filepath);
    auto origin = Director::getInstance() -> getVisibleOrigin();
    auto visibleSize = Director::getInstance() -> getVisibleSize();
    auto center = origin + Vec2(visibleSize.width / 2, visibleSize.height / 2);
    auto scaleX = visibleSize.width / background -> getContentSize().width;
    auto scaleY = visibleSize.height / background -> getContentSize().height;
    background -> setScale(scaleX, scaleY);
    background -> setPosition(center);

    return background;
}

void Game::InitializeGameNodes() {
    gameState_.setHealthPoints(3);
    gameState_.getLevel().ReadFromFile("levels/Level1.dat");
    
    gameState_.reset();
    updateScore(0);
    updateHealth(0);
}


void Game::updateHealth(int hit) {
    gameState_.hit(hit);
    if (healthSprites_.size() == 0) {
        Size visibleSize = Director::getInstance() -> getVisibleSize();
        Vec2 origin = Director::getInstance() -> getVisibleOrigin();
        Vec2 currentPosition = origin + Vec2(35, visibleSize.height - 35);
        for (size_t i = 0; i < gameState_.getHealthPoints(); i++) {
            // TODO: Singleton Image Manager ?
            auto sprite = Sprite::create("Images/heart.png");
            sprite -> setScale(0.25);
            addChild(sprite, 0);
            sprite -> setPosition(currentPosition);
            healthSprites_.pushBack(sprite);
            
            currentPosition += Vec2(50, 0);
        }
    } else {
        while (gameState_.getHealthPoints() < healthSprites_.size()) {
            auto emmiter = ParticleExplosion::create();
            emmiter -> setPosition(healthSprites_.back() -> getPosition());
            addChild(emmiter, 10);
            healthSprites_.back() -> removeFromParentAndCleanup(true);
            healthSprites_.popBack();
        }
    }
    
    if (!gameState_.isAlive()) {
        scheduleOnce(SEL_SCHEDULE(&Game::ShowFinishScene), 1);
    }
}


void Game::updateScore(size_t difference) {
    gameState_.updateScore(difference);
    maxScore_ = std::max(maxScore_, gameState_.getScore());
    
    if (scoreLabel_ == nullptr) {
        Vec2 origin = Director::getInstance() -> getVisibleOrigin();
        Size visibleSize = Director::getInstance() -> getVisibleSize();
    
        Vec2 position = origin + Vec2(visibleSize.width - GAME_FONTSIZE * 5,
                                  visibleSize.height - GAME_FONTSIZE);
        scoreLabel_ = CreateTextLabel("Score: 0", GAME_FONTPATH, GAME_FONTSIZE,
                                      position);
        position -= Vec2(0, GAME_FONTSIZE);
        maxScoreLabel_ = CreateTextLabel("Max: 0", GAME_FONTPATH, GAME_FONTSIZE,
                                      position);
    } else {
        scoreLabel_ -> setString("Score: " + std::to_string(gameState_.getScore()));
        maxScoreLabel_ -> setString("Max: " + std::to_string(maxScore_));
    }
}


void Game::FindDeadTasks() {
    auto allChildren = this -> getChildren();
    for (auto& child: allChildren) {
        if (child -> getPosition().y < 0 &&
                child -> getColor() != RIGHT_ANSWER_COLOR) {
            updateHealth(1);
            updateTask();
            child -> removeFromParentAndCleanup(true);
        }
    }
}


void Game::ShowFinishScene() {
    CreateFinishScene();
    Director::getInstance() -> pushScene(finishScene_);
}


void Game::CreateFinishScene() {
    finishScene_ = Scene::create();
    
    auto background = CreateBackground("Images/gameover.jpg");
    finishScene_ -> addChild(background, -50);
    
    Vector<MenuItem*> MenuItems;
    
    // Need images ;(
    auto replayItem = MenuItemImage::create("Images/replay.png",
                                            "Images/replay.png",
        [&] (Ref* sender) {
            Director::getInstance() -> popScene();
            Game::InitializeGameNodes();
        });
    auto closeItem = MenuItemImage::create("Images/close.png",
                                           "Images/close.png",
        [&] (Ref* sender) {
            Game::ExitPressed();
        });
    
    replayItem -> setScale(0.25);
    closeItem -> setScale(0.25);
    MenuItems.pushBack(replayItem);
    MenuItems.pushBack(closeItem);
    
    auto origin = Director::getInstance() -> getVisibleOrigin();
    auto size = Director::getInstance() -> getVisibleSize();
    auto position = origin + Vec2(size.width / 2, size.height / 2 - 180);
    
    auto menu = Menu::createWithArray(MenuItems);
    menu -> setPosition(position);
    menu -> alignItemsInColumns(2);
    finishScene_ -> addChild(menu);
}


void Game::ExitPressed() {
    Director::getInstance() -> end();
}


Label* Game::CreateTextLabel(const std::string& text,
    const std::string& fontFilePath, int fontSize, Vec2 position) {
    Label* label = Label::createWithTTF(text, fontFilePath, fontSize);
    label -> setPosition(position);
    this -> addChild(label, 1, "test_label");
    
    return label;
}


void Game::MoveTo(Sprite* object, Vec2 destination, float duration) const {
    auto moveAction = MoveTo::create(duration, destination);
    auto callback = CallFuncN::create([&] (Node* node) {
        node -> removeFromParentAndCleanup(true);
    });
    
    auto sequence = Sequence::create(moveAction, callback, nullptr);
    object -> runAction(sequence);
}


void Game::ScaleBy(Sprite* object, Vec2 destination, float duration) const {
    auto moveAction = ScaleBy::create(duration, destination.x, destination.y);
    object -> runAction(moveAction);
}


void Game::updateTask() {
    Size visibleSize = Director::getInstance() -> getVisibleSize();
    Vec2 origin = Director::getInstance() -> getVisibleOrigin();
    Vec2 start = origin + Vec2(visibleSize.width / 2,
                               visibleSize.height - GAME_FONTSIZE);
    Vec2 destination = origin + Vec2(visibleSize.width / 2, -GAME_FONTSIZE);
    
    auto task = gameState_.getLevel().getNextTask();
    currentTask_ = task.second;
    taskLabel_ = CreateTextLabel(task.second.getWord(), GAME_FONTPATH,
                             GAME_FONTSIZE, start);
    MoveTo((Sprite*)taskLabel_, destination, task.first);
}


void Game::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event) {
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio -> setEffectsVolume(100);
    
    if (currentTask_.isKeyCorrect(keyCode)) {
        taskLabel_ -> setString(currentTask_.getCorrect());
        taskLabel_ -> setColor(RIGHT_ANSWER_COLOR);
        ScaleBy((Sprite*)taskLabel_, Vec2(1.5, 1.5), 0.2);
        updateScore(gameState_.getLevel().getScorePerTask());
        scheduleOnce(SEL_SCHEDULE(&Game::updateTask), 1);
        
        audio -> playEffect("Sounds/goodanswer.mp3");
    } else {
        updateHealth(1);
        audio -> playEffect("Sounds/badanswer.WAV");
    }
}
