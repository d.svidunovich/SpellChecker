#include "Wordtask.h"


WordTask::WordTask(const std::string& word, const std::string& correct,
        const std::string& key): word_(word), correct_(correct), key_(key) {}


const std::string& WordTask::getWord() const {
    return word_;
}


void WordTask::setWord(const std::string& word) {
    word_ = word;
}


const std::string& WordTask::getCorrect() const {
    return correct_;
}


void WordTask::setCorrect(const std::string& correct) {
    correct_ = correct;
}


const std::string& WordTask::getKey() const {
    return key_;
}


void WordTask::setKey(const std::string& key) {
    key_ = key;
}


bool WordTask::isKeyCorrect(const std::string& key) const {
    return key_ == key;
}


bool WordTask::isKeyCorrect(cocos2d::EventKeyboard::KeyCode keyCode) const {
    int offset = (char)cocos2d::EventKeyboard::KeyCode::KEY_A - 'a';
    std::string key;
    key += (char)((int)keyCode - offset);
    
    return isKeyCorrect(key);
}


std::string WordTask::ToString() const {
    return word_ + " " + correct_ + " " + key_;
}
