#pragma once

#include "GameState.h"
#include <SimpleAudioEngine.h>

#define GAME_FONTPATH "fonts/Marker Felt.ttf"
#define GAME_FONTSIZE 26


class Game : public cocos2d::Layer
{
public:
    static const cocos2d::Color3B RIGHT_ANSWER_COLOR;
    static cocos2d::Scene* createScene();
    virtual bool init();

    CREATE_FUNC(Game);
    
private:
    cocos2d::Sprite* CreateBackground(const std::string& filepath);
    void InitializeGameNodes();
    void updateHealth(int hit);
    void updateScore(size_t difference);
    void FindDeadTasks();
    
    void ShowFinishScene();
    void CreateFinishScene();
    void ExitPressed();
    
    cocos2d::Label* CreateTextLabel(const std::string& text,
                                    const std::string& fontFilePath,
                                    int fontSize,
                                    cocos2d::Vec2 position);
    
    void MoveTo(cocos2d::Sprite* object, cocos2d::Vec2 destination,
                float duration) const;
    void ScaleBy(cocos2d::Sprite* object, cocos2d::Vec2 destination,
                 float duration) const;
    void updateTask();
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode,
                      cocos2d::Event* event);
    
    
    GameState gameState_;
    WordTask currentTask_;
    cocos2d::Label* taskLabel_;
    cocos2d::Label* scoreLabel_;
    cocos2d::Label* maxScoreLabel_;
    cocos2d::Vector<cocos2d::Sprite*> healthSprites_;
    cocos2d::Scene* finishScene_;
    size_t maxScore_;
};
