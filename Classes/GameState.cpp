#include "GameState.h"


GameState::GameState(): score_(0), healthPoints_(0) {}


size_t GameState::getScore() const {
    return score_;
}


bool GameState::isAlive() const {
    return healthPoints_ > 0;
}


int GameState::getHealthPoints() const {
    return healthPoints_;
}


void GameState::setHealthPoints(int healthPoints) {
    healthPoints_ = healthPoints;
}


Level& GameState::getLevel() {
    return level_;
}


void GameState::hit(int damage) {
    healthPoints_ = std::max(0, healthPoints_ - damage);
}


void GameState::updateScore(size_t cash) {
    score_ += cash;
}


void GameState::reset() {
    score_ = 0;
}
